1.Create a simple form with the following input fields - name, address, email, phone, website
- you should use the proper input types for each field
- the form must have the first field on autofocus when the page is loaded
- the form can be submited only if all fields are validated
2. The google maps form will have two functionalities:
2.1 If the submit button is clicked, and all of the information pass validation, a google
map pointing to the address used in the address form field should be loaded on the
right side of the form.The form must also save the user inputs inside an object in
local storage
2.2 There should be another button named “Find location manually”, with the following
behaviour:
- when the button is clicked, a new google map is opened on the right side of the form
- when the user clicks somewhere on the map, the input field of the form must become
populated with the selected address
3. Use MVC design pattern.
*Bonus: Bind the address input field with the original one from google api 