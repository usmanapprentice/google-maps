class Person {
	constructor(name, address, email, phone, web, coords) {
		this._name = name;
		this._address = address;
		this._email = email;
		this._phone = phone;
		this._web = web;
		this._coords = coords;
	}

	getName() {
		return this._name;
	}
	getAddress() {
		return this._address;
	}
	getEmail() {
		return this._email;
	}
	getPhone() {
		return this._phone;
	}
	getWeb() {
		return this._web;
	}
	getlatLng() {
		return this._coords;
	}
}
