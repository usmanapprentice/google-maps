$(document).ready(function() {
	// loadMapScript();
	var myMap = new GMapsHelper(45.8256, 25.6258);
	myMap.initMap();
	var validator = new Validator();
	var data = [];
	var person, address;
	var ErrMessage = [];
	var LatLng = {
		lat: '',
		lng:''
	};

	myMap.addressBar();

	var cacheDom = {
		$container: $('.container'),
		$wrapper: $('.wrapper'),
		$mapElement: $('#GoogleMap'),
		$errorElement:  $('#errors'),
		$sendDetails: $('#send'),
		$location: $('#location'),
		$name: $('#name'),
		$address: $('#address'),
		$phone: $('#phone'),
		$email: $('#email'),
		$web: $('#web'),
		$info: $('#information'),
		$input: $('input'),
		$warning: $('#warning'),
	};

	//Send Details click
	cacheDom.$sendDetails.click(function() {
		if (validator.checkValidation(cacheDom.$name, cacheDom.$address, cacheDom.$email, cacheDom.$phone, cacheDom.$web)) {
			cacheDom.$errorElement.text('');
			alterMapCSS();
			cacheDom.$info.show('slow');
			geocodeAddress(cacheDom.$address.val(), LatLng, myMap);
			person = new Person(
							cacheDom.$name.val(),
							cacheDom.$address.val(),
							cacheDom.$email.val(),
							cacheDom.$phone.val(),
							cacheDom.$web.val(),
							LatLng
						);

			data.push(person);
			saveUserData(data[data.length-1]);
		}
	});

	// Save userData to local storage.
	function saveUserData (data) {
		for (var prop in data) {
			$('.info' + prop).html(data[prop]);
			console.log('  ' + prop + ': ' + data[prop]);
			localStorage.setItem(prop, data[prop]);
		}
		cacheDom.$input.hide('slow');
		cacheDom.$warning.text('information saved');
	}

	// find location manually
	cacheDom.$location.click(function() {
		myMap.getLocation();
		let regtext = /Drag the marker to your positon/;
		if (!cacheDom.$container.text().match(regtext)) {
			cacheDom.$container.append('<h3>Drag the marker to your positon.</h3>');
		}
		alterMapCSS();
	});

	function alterMapCSS() {
		cacheDom.$wrapper.addClass("map-enabled");
		cacheDom.$mapElement.css( {
			display: 'block'
		});
	}

	function loadMapScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBcJEVdmzRmtIKtCit5Vdcnh9BF1dAEfBs&libraries=places';
        document.body.appendChild(script);
    }

});
