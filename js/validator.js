class Validator {
    validateName (name) {
	    var nameReg = /^[A-Za-z]{3,10}$/;
	    if (!name.match(nameReg)) {
	    	return false;
	    }
	    return true;
    }

    validateEmail (email) {
        var emailReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!email.match(emailReg)) {
            return false;
        }
        return true;
    }

    validatePhone (phone) {
    	var telReg = /^\d{10}$/;
    	if (!phone.match(telReg)) {
        	return false;
      	}
      	return true;
    }
    validateWeb (url) {
		var urlReg = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;
		if (!url.match(urlReg)) {
			return false;
		}
		return true;
    }

    validateAddress (address) {
		if (address === '') {
			return false;
		}
		return true;
    }

    checkValidation(name, address, email, phone, web) {
        this.validName = this.validateName(name.val()),
        this.validAddress = this.validateAddress(address.val()),
        this.validEmail = this.validateEmail(email.val()),
        this.validPhone = this.validatePhone(phone.val()),
        this.validWeb = this.validateWeb(web.val());

        if(!this.validName) {
            $('#error1').show();
        } else {
            $('#error1').hide();
        }
        if(!this.validAddress) {
            $('#error2').show();
        } else {
            $('#error2').hide();
        }
        if(!this.validEmail) {
            $('#error3').show();
        } else {
            $('#error3').hide();
        }
        if(!this.validPhone) {
            $('#error4').show();
        } else {
            $('#error4').hide();
        }
        if(!this.validWeb) {
            $('#error5').show();
        } else {
            $('#error5').hide();
        }

        if(this.validName && this.validAddress && this.validEmail && this.validPhone && this.validWeb) {
            return true;
        }
        return false;
    }

}
