class GMapsHelper {
    constructor(mapX, mapY) {
        this.mapX = mapX;
        this.mapY = mapY;
        this.map ;
        this.marker;
        this.mapProp = {
            center: function () {
                return new google.maps.LatLng(mapX,mapY);
            },
            zoom: 15,
        }

    }

    addressBar(){
      return new google.maps.places.SearchBox(document.getElementById('address'));
    }


    initMap(mapX, mapY){
        var mapProp = {
          center: new google.maps.LatLng(mapX,mapY),
          zoom: 15,
        }
        this.map = new google.maps.Map(document.getElementById("GoogleMap"),mapProp);
        showMarker(this.map, mapProp);
    }


    getLocation() {
    	if (navigator.geolocation) {
    		navigator.geolocation.getCurrentPosition(onPositionReceived.bind(this), locationNotReceived.bind(this));
    	}
        return;
    }
}




function onPositionReceived (position) {
    this.mapX =  position.coords.latitude;
    this.mapY = position.coords.longitude;
    address = getAddressfromLatLng(this.mapX, this.mapY);
    $('#address').val(address);
    this.initMap(this.mapX, this.mapY);
}

function locationNotReceived (positionError) {
    console.log(positionError);
}

function getAddressfromLatLng(mapX, mapY) {
    $.ajax( { url:'http://maps.googleapis.com/maps/api/geocode/json?latlng='+mapX+','+mapY+'&sensor=true',
        success: function(data) {
            let  formattedAddress = data.results[0].formatted_address;
            $('#address').val(formattedAddress);
            return formattedAddress;
        }
    });
}

function showMarker(map, mapProp) {
      var marker = new google.maps.Marker( {
    	     position:mapProp.center,
    	     map:map,
    	     draggable:true
	   });
     google.maps.event.addListener(marker, 'dragend', markerDrag);
}

var markerDrag = function() {
 	getAddressfromLatLng(this.position.lat(), this.position.lng());
}

function geocodeAddress (address, LatLng, myMap) {
	let geocoder = new google.maps.Geocoder();
	let coords_obj;
	geocoder.geocode( {address:address}, function (results, status) {
		coords_obj = results[0].geometry.location;
		LatLng.lat=coords_obj.lat();
		LatLng.lng = coords_obj.lng();
		userAddress = results[0].formatted_address;
		if (address != userAddress) {
			$('#address').val(userAddress);
		}
		myMap.initMap(coords_obj.lat(), coords_obj.lng());
	});
}
